<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="uk" sourcelanguage="">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="config.py" line="18"/>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Automatically save documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore window geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore live preview state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open external links in ReText window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open unknown files in plain text mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Highlight current line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Show line numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tab key inserts spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tabulation width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Display right margin at column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Icon theme name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="window.py" line="18"/>
        <source>Enter locale name (example: en_US)</source>
        <translation>Введіть код мови (наприклад: uk_UA)</translation>
    </message>
    <message>
        <location filename="window.py" line="23"/>
        <source>Set as default</source>
        <translation>По замовчуванню</translation>
    </message>
</context>
<context>
    <name>ReTextWindow</name>
    <message>
        <location filename="window.py" line="72"/>
        <source>File toolbar</source>
        <translation>Панель файлу</translation>
    </message>
    <message>
        <location filename="window.py" line="74"/>
        <source>Edit toolbar</source>
        <translation>Панель редагування</translation>
    </message>
    <message>
        <location filename="window.py" line="76"/>
        <source>Search toolbar</source>
        <translation>Панель пошуку</translation>
    </message>
    <message>
        <location filename="window.py" line="81"/>
        <source>New</source>
        <translation>Новий</translation>
    </message>
    <message>
        <location filename="window.py" line="84"/>
        <source>Open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <location filename="window.py" line="87"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="window.py" line="94"/>
        <source>Save as</source>
        <translation>Зберегти як</translation>
    </message>
    <message>
        <location filename="window.py" line="96"/>
        <source>Print</source>
        <translation>Друкувати</translation>
    </message>
    <message>
        <location filename="window.py" line="99"/>
        <source>Print preview</source>
        <translation>Попередній перегляд перед друком</translation>
    </message>
    <message>
        <location filename="window.py" line="101"/>
        <source>View HTML code</source>
        <translation>Переглянути код HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="102"/>
        <source>Change default font</source>
        <translation>Змінити шрифт по замовчуванню</translation>
    </message>
    <message>
        <location filename="window.py" line="103"/>
        <source>Find text</source>
        <translation>Знайти текст</translation>
    </message>
    <message>
        <location filename="window.py" line="108"/>
        <source>Preview</source>
        <translation>Перегляд</translation>
    </message>
    <message>
        <location filename="window.py" line="117"/>
        <source>Live preview</source>
        <translation>Одночасний перегляд</translation>
    </message>
    <message>
        <location filename="window.py" line="119"/>
        <source>Fullscreen mode</source>
        <translation>На весь екран</translation>
    </message>
    <message>
        <location filename="window.py" line="127"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location filename="window.py" line="130"/>
        <source>Undo</source>
        <translation>Відмінити дію</translation>
    </message>
    <message>
        <location filename="window.py" line="132"/>
        <source>Redo</source>
        <translation>Повторити дію</translation>
    </message>
    <message>
        <location filename="window.py" line="134"/>
        <source>Copy</source>
        <translation>Скопіювати</translation>
    </message>
    <message>
        <location filename="window.py" line="136"/>
        <source>Cut</source>
        <translation>Вирізати</translation>
    </message>
    <message>
        <location filename="window.py" line="138"/>
        <source>Paste</source>
        <translation>Вставити</translation>
    </message>
    <message>
        <location filename="window.py" line="148"/>
        <source>Enable</source>
        <translation>Увімкнути</translation>
    </message>
    <message>
        <location filename="window.py" line="149"/>
        <source>Set locale</source>
        <translation>Вибрати мову</translation>
    </message>
    <message>
        <location filename="window.py" line="150"/>
        <source>Plain text</source>
        <translation>Простий текст</translation>
    </message>
    <message>
        <location filename="window.py" line="152"/>
        <source>Use WebKit renderer</source>
        <translation>Використовувати WebKit</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Generate webpages</source>
        <translation>Створити веб-сторінки</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Show</source>
        <translation>Показати</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Next</source>
        <translation>Наступне</translation>
    </message>
    <message>
        <location filename="window.py" line="162"/>
        <source>Previous</source>
        <translation>Попереднє</translation>
    </message>
    <message>
        <location filename="window.py" line="164"/>
        <source>Get help online</source>
        <translation>Отримати довідку в мережі</translation>
    </message>
    <message>
        <location filename="window.py" line="165"/>
        <source>About %s</source>
        <comment>Example of final string: About ReText</comment>
        <translation>Про %s</translation>
    </message>
    <message>
        <location filename="window.py" line="169"/>
        <source>About Qt</source>
        <translation>Про Qt</translation>
    </message>
    <message>
        <location filename="window.py" line="190"/>
        <source>Bold</source>
        <translation>Потовщення</translation>
    </message>
    <message>
        <location filename="window.py" line="192"/>
        <source>Italic</source>
        <translation>Нахил</translation>
    </message>
    <message>
        <location filename="window.py" line="194"/>
        <source>Underline</source>
        <translation>Підкреслення</translation>
    </message>
    <message>
        <location filename="window.py" line="202"/>
        <source>Tags</source>
        <translation>Теги</translation>
    </message>
    <message>
        <location filename="window.py" line="206"/>
        <source>Symbols</source>
        <translation>Символи</translation>
    </message>
    <message>
        <location filename="window.py" line="220"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="window.py" line="221"/>
        <source>Edit</source>
        <translation>Редагування</translation>
    </message>
    <message>
        <location filename="window.py" line="222"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="window.py" line="225"/>
        <source>Open recent</source>
        <translation>Відкрити останній</translation>
    </message>
    <message>
        <location filename="window.py" line="229"/>
        <source>Directory</source>
        <translation>Тека</translation>
    </message>
    <message>
        <location filename="window.py" line="236"/>
        <source>Export</source>
        <translation>Експорт</translation>
    </message>
    <message>
        <location filename="window.py" line="257"/>
        <source>Spell check</source>
        <translation>Перевірка правопису</translation>
    </message>
    <message>
        <location filename="window.py" line="268"/>
        <source>Formatting</source>
        <translation>Форматування</translation>
    </message>
    <message>
        <location filename="window.py" line="306"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="window.py" line="308"/>
        <source>Case sensitively</source>
        <translation>Врахувати регістр</translation>
    </message>
    <message>
        <location filename="window.py" line="1130"/>
        <source>New document</source>
        <translation>Новий документ</translation>
    </message>
    <message>
        <location filename="window.py" line="654"/>
        <source>Could not parse file contents, check if you have the &lt;a href=&quot;%s&quot;&gt;necessary module&lt;/a&gt; installed!</source>
        <translation>Неможливо розпізнати вміст фалу, перевірте наявність &lt;a href=&quot;%s&quot;&gt;необхідних модулів&lt;/a&gt;!</translation>
    </message>
    <message>
        <location filename="window.py" line="759"/>
        <source>Please, save the file somewhere.</source>
        <translation>Будь ласка, збережіть кудись файл.</translation>
    </message>
    <message>
        <location filename="window.py" line="744"/>
        <source>Failed to copy default template, please create template.html manually.</source>
        <translation>Не вдалось скопіювати шаблон по замовчуванню, будь ласка, створіть  template.html самостійно.</translation>
    </message>
    <message>
        <location filename="window.py" line="748"/>
        <source>Webpages saved in &lt;code&gt;html&lt;/code&gt; directory.</source>
        <translation>Веб-сторінки збережено в &lt;code&gt;html&lt;/code&gt; теці.</translation>
    </message>
    <message>
        <location filename="window.py" line="750"/>
        <source>Show directory</source>
        <translation>Переглянути теку</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Select one or several files to open</source>
        <translation>Виберіть один або кілька файлів</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Supported files</source>
        <translation>Файли що підтримуються</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>All files (*)</source>
        <translation>Всі файли (*)</translation>
    </message>
    <message>
        <location filename="window.py" line="956"/>
        <source>Plain text (*.txt)</source>
        <translation>Простий текст (*.txt)</translation>
    </message>
    <message>
        <location filename="window.py" line="959"/>
        <source>%s files</source>
        <comment>Example of final string: Markdown files</comment>
        <translation>Файли %s</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>Save file</source>
        <translation>Зберегти файл</translation>
    </message>
    <message>
        <location filename="window.py" line="978"/>
        <source>Cannot save to file because it is read-only!</source>
        <translation>Неможливо зберегти, файл тільки для читання!</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>Export document to ODT</source>
        <translation>Експортувати як ODT</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>OpenDocument text files (*.odt)</source>
        <translation>Текстові файли OpenDocument (*.odt)</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>HTML files (*.html *.htm)</source>
        <translation>Файли HTML (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>Export document to PDF</source>
        <translation>Експортувати як PDF</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>PDF files (*.pdf)</source>
        <translation>Файли PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="window.py" line="1070"/>
        <source>Print document</source>
        <translation>Друкувати документ</translation>
    </message>
    <message>
        <location filename="window.py" line="1091"/>
        <source>Export document</source>
        <translation>Експортувати документ</translation>
    </message>
    <message>
        <location filename="window.py" line="1109"/>
        <source>Failed to execute the command:</source>
        <translation>Помилка при виконанні команди:</translation>
    </message>
    <message>
        <location filename="window.py" line="1186"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Документ було змінено.
Бажаєте зберегти ці зміни?</translation>
    </message>
    <message>
        <location filename="window.py" line="1212"/>
        <source>HTML code</source>
        <translation>Код HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Simple but powerful editor for Markdown and reStructuredText</source>
        <translation>Простий але потужний текстовий редактор для Markdown та reStructuredText
</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Author: Dmitry Shachnev, 2011</source>
        <translation>Автор: Dmitry Shachnev, 2011</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Website</source>
        <translation>Веб-сайт</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Markdown syntax</source>
        <translation>Синтаксис Markdown</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>reStructuredText syntax</source>
        <translation>Синтаксис reStructuredText</translation>
    </message>
    <message>
        <location filename="window.py" line="265"/>
        <source>Default markup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="580"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="89"/>
        <source>Set encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="931"/>
        <source>Select file encoding from the list:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
