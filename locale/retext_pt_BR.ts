<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="pt_BR" sourcelanguage="">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="config.py" line="18"/>
        <source>Behavior</source>
        <translation>Comportamento</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Automatically save documents</source>
        <translation>Salvar documentos automaticamente</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore window geometry</source>
        <translation>Restaurar a geometria da janela</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore live preview state</source>
        <translation>Restaurar estado de visualização automática</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open external links in ReText window</source>
        <translation>Abrir links externos na janela ReText</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open unknown files in plain text mode</source>
        <translation>Abrir arquivos desconhecidos em modo de textpo simples</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Highlight current line</source>
        <translation>Destacar linha atual</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Show line numbers</source>
        <translation>Mostrar números de linhas</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tab key inserts spaces</source>
        <translation>Tab insere espaços</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tabulation width</source>
        <translation>Tamanho da tabulação</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Display right margin at column</source>
        <translation>Mostar margem direita na coluna</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Icon theme name</source>
        <translation>Nome do ícone tema</translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="window.py" line="18"/>
        <source>Enter locale name (example: en_US)</source>
        <translation>Forneça o nome da localidade (exemplo: pt_BR)</translation>
    </message>
    <message>
        <location filename="window.py" line="23"/>
        <source>Set as default</source>
        <translation>Definir como padrão</translation>
    </message>
</context>
<context>
    <name>ReTextWindow</name>
    <message>
        <location filename="window.py" line="72"/>
        <source>File toolbar</source>
        <translation>Barra de ferramentas Arquivo</translation>
    </message>
    <message>
        <location filename="window.py" line="74"/>
        <source>Edit toolbar</source>
        <translation>Barra de ferramentas Editar</translation>
    </message>
    <message>
        <location filename="window.py" line="76"/>
        <source>Search toolbar</source>
        <translation>Barra de ferramentas Pesquisar</translation>
    </message>
    <message>
        <location filename="window.py" line="81"/>
        <source>New</source>
        <translation>Novo</translation>
    </message>
    <message>
        <location filename="window.py" line="84"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="window.py" line="87"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="window.py" line="89"/>
        <source>Set encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="94"/>
        <source>Save as</source>
        <translation>Salvar como</translation>
    </message>
    <message>
        <location filename="window.py" line="96"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="window.py" line="99"/>
        <source>Print preview</source>
        <translation>Visualizar impressão</translation>
    </message>
    <message>
        <location filename="window.py" line="101"/>
        <source>View HTML code</source>
        <translation>Visualizar código HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="102"/>
        <source>Change default font</source>
        <translation>Trocar fonte padrão</translation>
    </message>
    <message>
        <location filename="window.py" line="103"/>
        <source>Find text</source>
        <translation>Encontrar texto</translation>
    </message>
    <message>
        <location filename="window.py" line="108"/>
        <source>Preview</source>
        <translation>Visualizar</translation>
    </message>
    <message>
        <location filename="window.py" line="117"/>
        <source>Live preview</source>
        <translation>Visualização automática</translation>
    </message>
    <message>
        <location filename="window.py" line="119"/>
        <source>Fullscreen mode</source>
        <translation>Modo tela cheia</translation>
    </message>
    <message>
        <location filename="window.py" line="580"/>
        <source>Preferences</source>
        <translation>Preferências</translation>
    </message>
    <message>
        <location filename="window.py" line="127"/>
        <source>Quit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="window.py" line="130"/>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <location filename="window.py" line="132"/>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
    <message>
        <location filename="window.py" line="134"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="window.py" line="136"/>
        <source>Cut</source>
        <translation>Recortar</translation>
    </message>
    <message>
        <location filename="window.py" line="138"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="window.py" line="148"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
    <message>
        <location filename="window.py" line="149"/>
        <source>Set locale</source>
        <translation>Definir localidade</translation>
    </message>
    <message>
        <location filename="window.py" line="150"/>
        <source>Plain text</source>
        <translation>Texto simples</translation>
    </message>
    <message>
        <location filename="window.py" line="152"/>
        <source>Use WebKit renderer</source>
        <translation>Utilizar renderizador WebKit</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Generate webpages</source>
        <translation>Gerar páginas web</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Show</source>
        <translation>Exibir</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Next</source>
        <translation>Seguinte</translation>
    </message>
    <message>
        <location filename="window.py" line="162"/>
        <source>Previous</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="window.py" line="164"/>
        <source>Get help online</source>
        <translation>Ajuda online</translation>
    </message>
    <message>
        <location filename="window.py" line="165"/>
        <source>About %s</source>
        <comment>Example of final string: About ReText</comment>
        <translation>Sobre %s</translation>
    </message>
    <message>
        <location filename="window.py" line="169"/>
        <source>About Qt</source>
        <translation>Sobre Qt</translation>
    </message>
    <message>
        <location filename="window.py" line="190"/>
        <source>Bold</source>
        <translation>Negrito</translation>
    </message>
    <message>
        <location filename="window.py" line="192"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="window.py" line="194"/>
        <source>Underline</source>
        <translation>Sobrescrito</translation>
    </message>
    <message>
        <location filename="window.py" line="202"/>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
    <message>
        <location filename="window.py" line="206"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="window.py" line="220"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="window.py" line="221"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="window.py" line="222"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="window.py" line="225"/>
        <source>Open recent</source>
        <translation>Abrir recente</translation>
    </message>
    <message>
        <location filename="window.py" line="229"/>
        <source>Directory</source>
        <translation>Diretório</translation>
    </message>
    <message>
        <location filename="window.py" line="236"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="window.py" line="257"/>
        <source>Spell check</source>
        <translation>Correção ortográfica</translation>
    </message>
    <message>
        <location filename="window.py" line="265"/>
        <source>Default markup</source>
        <translation>Markup padrão</translation>
    </message>
    <message>
        <location filename="window.py" line="268"/>
        <source>Formatting</source>
        <translation>Formatar</translation>
    </message>
    <message>
        <location filename="window.py" line="306"/>
        <source>Search</source>
        <translation>Pesquisar</translation>
    </message>
    <message>
        <location filename="window.py" line="308"/>
        <source>Case sensitively</source>
        <translation>Diferenciando maiúsculas e minúsculas</translation>
    </message>
    <message>
        <location filename="window.py" line="1130"/>
        <source>New document</source>
        <translation>Novo documento</translation>
    </message>
    <message>
        <location filename="window.py" line="654"/>
        <source>Could not parse file contents, check if you have the &lt;a href=&quot;%s&quot;&gt;necessary module&lt;/a&gt; installed!</source>
        <translation>Não foi possível analisar conteúdo do arquivo, verifique se você tem o &lt;a href=&quot;%s&quot;&gt;modulo necessário&lt;/a&gt; instalado!</translation>
    </message>
    <message>
        <location filename="window.py" line="759"/>
        <source>Please, save the file somewhere.</source>
        <translation>Por favor, salve o arquivo em outra localização.</translation>
    </message>
    <message>
        <location filename="window.py" line="744"/>
        <source>Failed to copy default template, please create template.html manually.</source>
        <translation>Erro ao copiar template padrão, por favor crie o arquivo template.html manualmente.</translation>
    </message>
    <message>
        <location filename="window.py" line="748"/>
        <source>Webpages saved in &lt;code&gt;html&lt;/code&gt; directory.</source>
        <translation>Páginas web salvas no diretório &lt;code&gt;html&lt;/code&gt;. </translation>
    </message>
    <message>
        <location filename="window.py" line="750"/>
        <source>Show directory</source>
        <translation>Exibir diretório</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Select one or several files to open</source>
        <translation>Selecione um ou mais arquivos para abrir</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Supported files</source>
        <translation>Arquivos suportados</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="window.py" line="931"/>
        <source>Select file encoding from the list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="956"/>
        <source>Plain text (*.txt)</source>
        <translation>Texto simples (*.txt)</translation>
    </message>
    <message>
        <location filename="window.py" line="959"/>
        <source>%s files</source>
        <comment>Example of final string: Markdown files</comment>
        <translation>%s arquivos</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>Save file</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="window.py" line="978"/>
        <source>Cannot save to file because it is read-only!</source>
        <translation>Não é possível salvar arquivo somente leitura!</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>Export document to ODT</source>
        <translation>Exportar documento para ODT </translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>OpenDocument text files (*.odt)</source>
        <translation>Arquivos de texto OpenDocument (*.odt)</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>HTML files (*.html *.htm)</source>
        <translation>Arquivos HTML (*.html, *.htm)</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>Export document to PDF</source>
        <translation>Exportar documento para PDF</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>PDF files (*.pdf)</source>
        <translation>Arquivos PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="window.py" line="1070"/>
        <source>Print document</source>
        <translation>Imprimir documento</translation>
    </message>
    <message>
        <location filename="window.py" line="1091"/>
        <source>Export document</source>
        <translation>Exportar documento</translation>
    </message>
    <message>
        <location filename="window.py" line="1109"/>
        <source>Failed to execute the command:</source>
        <translation>Falha ao executar o comando:</translation>
    </message>
    <message>
        <location filename="window.py" line="1186"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>O documento foi modificado.
Deseja salvar as alterações?</translation>
    </message>
    <message>
        <location filename="window.py" line="1212"/>
        <source>HTML code</source>
        <translation>Código HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Simple but powerful editor for Markdown and reStructuredText</source>
        <translation>Simples e poderoso editor para Markdown e reStructuredText</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Author: Dmitry Shachnev, 2011</source>
        <translation>Autor: Dmitry Shachnev, 2011</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Markdown syntax</source>
        <translation>Sintaxe Markdown</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>reStructuredText syntax</source>
        <translation>Sintaxe reStructuredText</translation>
    </message>
</context>
</TS>
