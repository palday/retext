<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ru_RU" sourcelanguage="">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="config.py" line="18"/>
        <source>Behavior</source>
        <translation>Поведение</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Automatically save documents</source>
        <translation>Автоматически сохранять документы</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore window geometry</source>
        <translation>Сохранять геометрию окна</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore live preview state</source>
        <translation>Сохранять состояние живого просмотра</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open external links in ReText window</source>
        <translation>Открывать внешние ссылки в окне ReText</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open unknown files in plain text mode</source>
        <translation>Открывать неизвестные файлы в режиме простого текста</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Editor</source>
        <translation>Редактор</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Highlight current line</source>
        <translation>Подсвечивать текущую строку</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Show line numbers</source>
        <translation>Показывать номера строк</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tab key inserts spaces</source>
        <translation>Tab вставляет пробелы</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tabulation width</source>
        <translation>Ширина табуляции</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Display right margin at column</source>
        <translation>Показывать разделитель в столбце</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Interface</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Icon theme name</source>
        <translation>Тема значков</translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="window.py" line="18"/>
        <source>Enter locale name (example: en_US)</source>
        <translation>Введите код локали (например: en_US)</translation>
    </message>
    <message>
        <location filename="window.py" line="23"/>
        <source>Set as default</source>
        <translation>Установить по умолчанию</translation>
    </message>
</context>
<context>
    <name>ReTextWindow</name>
    <message>
        <location filename="window.py" line="84"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="window.py" line="87"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="window.py" line="96"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="window.py" line="108"/>
        <source>Preview</source>
        <translation>Просмотр</translation>
    </message>
    <message>
        <location filename="window.py" line="202"/>
        <source>Tags</source>
        <translation>Теги</translation>
    </message>
    <message>
        <location filename="window.py" line="206"/>
        <source>Symbols</source>
        <translation>Символы</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>HTML files (*.html *.htm)</source>
        <translation>Файлы HTML (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="window.py" line="94"/>
        <source>Save as</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="window.py" line="127"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="window.py" line="169"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="window.py" line="220"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="window.py" line="221"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="window.py" line="222"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="window.py" line="236"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="window.py" line="72"/>
        <source>File toolbar</source>
        <translation>Панель файла</translation>
    </message>
    <message>
        <location filename="window.py" line="74"/>
        <source>Edit toolbar</source>
        <translation>Панель редактирования</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>Export document to PDF</source>
        <translation>Экспортировать документ как PDF</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>PDF files (*.pdf)</source>
        <translation>Файлы PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="window.py" line="1186"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Документ был изменён.
Сохранить изменения?</translation>
    </message>
    <message>
        <location filename="window.py" line="130"/>
        <source>Undo</source>
        <translation>Отменить действие</translation>
    </message>
    <message>
        <location filename="window.py" line="132"/>
        <source>Redo</source>
        <translation>Повторить действие</translation>
    </message>
    <message>
        <location filename="window.py" line="134"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="window.py" line="136"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="window.py" line="138"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="window.py" line="1130"/>
        <source>New document</source>
        <translation>Новый документ</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="window.py" line="81"/>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <location filename="window.py" line="225"/>
        <source>Open recent</source>
        <translation>Открыть последние</translation>
    </message>
    <message>
        <location filename="window.py" line="150"/>
        <source>Plain text</source>
        <translation>Простой текст</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>OpenDocument text files (*.odt)</source>
        <translation>Файлы текста OpenDocument (*.odt)</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>Export document to ODT</source>
        <translation>Экспортировать документ как ODT</translation>
    </message>
    <message>
        <location filename="window.py" line="101"/>
        <source>View HTML code</source>
        <translation>Просмотр кода HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="1212"/>
        <source>HTML code</source>
        <translation>код HTML</translation>
    </message>
    <message>
        <location filename="window.py" line="99"/>
        <source>Print preview</source>
        <translation>Предпросмотр печати</translation>
    </message>
    <message>
        <location filename="window.py" line="1070"/>
        <source>Print document</source>
        <translation>Распечатать документ</translation>
    </message>
    <message>
        <location filename="window.py" line="117"/>
        <source>Live preview</source>
        <translation>Живой просмотр</translation>
    </message>
    <message>
        <location filename="window.py" line="748"/>
        <source>Webpages saved in &lt;code&gt;html&lt;/code&gt; directory.</source>
        <translation>Веб-страницы сохранены в папке &lt;code&gt;html&lt;/code&gt;.</translation>
    </message>
    <message>
        <location filename="window.py" line="759"/>
        <source>Please, save the file somewhere.</source>
        <translation>Сначала сохраните файл.</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Generate webpages</source>
        <translation>Создать веб-страницы</translation>
    </message>
    <message>
        <location filename="window.py" line="956"/>
        <source>Plain text (*.txt)</source>
        <translation>Простой текст (*.txt)</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="window.py" line="229"/>
        <source>Directory</source>
        <translation>Папка</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Supported files</source>
        <translation>Поддерживаемые файлы</translation>
    </message>
    <message>
        <location filename="window.py" line="102"/>
        <source>Change default font</source>
        <translation>Изменить стандартный шрифт</translation>
    </message>
    <message>
        <location filename="window.py" line="1091"/>
        <source>Export document</source>
        <translation>Экспортировать документ</translation>
    </message>
    <message>
        <location filename="window.py" line="148"/>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="window.py" line="149"/>
        <source>Set locale</source>
        <translation>Установить локаль</translation>
    </message>
    <message>
        <location filename="window.py" line="257"/>
        <source>Spell check</source>
        <translation>Проверка орфографии</translation>
    </message>
    <message>
        <location filename="window.py" line="119"/>
        <source>Fullscreen mode</source>
        <translation>Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="window.py" line="76"/>
        <source>Search toolbar</source>
        <translation>Панель поиска</translation>
    </message>
    <message>
        <location filename="window.py" line="103"/>
        <source>Find text</source>
        <translation>Поиск текста</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Next</source>
        <translation>Следующее</translation>
    </message>
    <message>
        <location filename="window.py" line="162"/>
        <source>Previous</source>
        <translation>Предыдущее</translation>
    </message>
    <message>
        <location filename="window.py" line="306"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="window.py" line="308"/>
        <source>Case sensitively</source>
        <translation>Учитывать регистр</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Author: Dmitry Shachnev, 2011</source>
        <translation>Автор: Dmitry Shachnev, 2011</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Website</source>
        <translation>Веб-сайт</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Markdown syntax</source>
        <translation>Синтаксис Markdown</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>reStructuredText syntax</source>
        <translation>Синтаксис reStructuredText</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Simple but powerful editor for Markdown and reStructuredText</source>
        <translation>Простой, но мощный редактор для Markdown и reStructuredText</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Select one or several files to open</source>
        <translation>Выберите один или несколько файлов для открытия</translation>
    </message>
    <message>
        <location filename="window.py" line="164"/>
        <source>Get help online</source>
        <translation>Получить помощь в интернете</translation>
    </message>
    <message>
        <location filename="window.py" line="750"/>
        <source>Show directory</source>
        <translation>Открыть папку</translation>
    </message>
    <message>
        <location filename="window.py" line="978"/>
        <source>Cannot save to file because it is read-only!</source>
        <translation>Невозможно сохранить в файл, так как он доступен только для чтения!</translation>
    </message>
    <message>
        <location filename="window.py" line="190"/>
        <source>Bold</source>
        <translation>Полужирный</translation>
    </message>
    <message>
        <location filename="window.py" line="192"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="window.py" line="194"/>
        <source>Underline</source>
        <translation>Подчёркивание</translation>
    </message>
    <message>
        <location filename="window.py" line="268"/>
        <source>Formatting</source>
        <translation>Форматирование</translation>
    </message>
    <message>
        <location filename="window.py" line="165"/>
        <source>About %s</source>
        <comment>Example of final string: About ReText</comment>
        <translation>О %s</translation>
    </message>
    <message>
        <location filename="window.py" line="152"/>
        <source>Use WebKit renderer</source>
        <translation>Использовать движок WebKit</translation>
    </message>
    <message>
        <location filename="window.py" line="1109"/>
        <source>Failed to execute the command:</source>
        <translation>Невозможно запустить команду:</translation>
    </message>
    <message>
        <location filename="window.py" line="959"/>
        <source>%s files</source>
        <comment>Example of final string: Markdown files</comment>
        <translation>Файлы %s</translation>
    </message>
    <message>
        <location filename="window.py" line="744"/>
        <source>Failed to copy default template, please create template.html manually.</source>
        <translation>Не удалось скопировать шаблон по умолчанию, создайте template.html вручную.</translation>
    </message>
    <message>
        <location filename="window.py" line="654"/>
        <source>Could not parse file contents, check if you have the &lt;a href=&quot;%s&quot;&gt;necessary module&lt;/a&gt; installed!</source>
        <translation>Не удалось обработать содержимое файла, убедитесь, что установлен &lt;a href=&quot;%s&quot;&gt;необходимый модуль&lt;/a&gt;!</translation>
    </message>
    <message>
        <location filename="window.py" line="265"/>
        <source>Default markup</source>
        <translation>Язык разметки по умолчанию</translation>
    </message>
    <message>
        <location filename="window.py" line="580"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="window.py" line="89"/>
        <source>Set encoding</source>
        <translation>Установить кодировку</translation>
    </message>
    <message>
        <location filename="window.py" line="931"/>
        <source>Select file encoding from the list:</source>
        <translation>Выберите кодировку файла из списка:</translation>
    </message>
</context>
</TS>
