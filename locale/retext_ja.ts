<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ja_JP" sourcelanguage="">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="config.py" line="18"/>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Automatically save documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore window geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore live preview state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open external links in ReText window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open unknown files in plain text mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Highlight current line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Show line numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tab key inserts spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tabulation width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Display right margin at column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Icon theme name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="window.py" line="18"/>
        <source>Enter locale name (example: en_US)</source>
        <translation>ロケール名を入力してください (例：ja_JP)</translation>
    </message>
    <message>
        <location filename="window.py" line="23"/>
        <source>Set as default</source>
        <translation>デフォルトにする</translation>
    </message>
</context>
<context>
    <name>ReTextWindow</name>
    <message>
        <location filename="window.py" line="72"/>
        <source>File toolbar</source>
        <translation>ファイルツールバー</translation>
    </message>
    <message>
        <location filename="window.py" line="74"/>
        <source>Edit toolbar</source>
        <translation>編集ツールバー</translation>
    </message>
    <message>
        <location filename="window.py" line="76"/>
        <source>Search toolbar</source>
        <translation>検索ツールバー</translation>
    </message>
    <message>
        <location filename="window.py" line="81"/>
        <source>New</source>
        <translation>新規</translation>
    </message>
    <message>
        <location filename="window.py" line="84"/>
        <source>Open</source>
        <translation>開く</translation>
    </message>
    <message>
        <location filename="window.py" line="87"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="window.py" line="94"/>
        <source>Save as</source>
        <translation>名前を付けて保存</translation>
    </message>
    <message>
        <location filename="window.py" line="96"/>
        <source>Print</source>
        <translation>印刷</translation>
    </message>
    <message>
        <location filename="window.py" line="99"/>
        <source>Print preview</source>
        <translation>印刷プレビュー</translation>
    </message>
    <message>
        <location filename="window.py" line="101"/>
        <source>View HTML code</source>
        <translation>HTMLコードを表示</translation>
    </message>
    <message>
        <location filename="window.py" line="102"/>
        <source>Change default font</source>
        <translation>規定のフォントを変更</translation>
    </message>
    <message>
        <location filename="window.py" line="103"/>
        <source>Find text</source>
        <translation>テキストを検索</translation>
    </message>
    <message>
        <location filename="window.py" line="108"/>
        <source>Preview</source>
        <translation>プレビュー</translation>
    </message>
    <message>
        <location filename="window.py" line="117"/>
        <source>Live preview</source>
        <translation>ライブプレビュー</translation>
    </message>
    <message>
        <location filename="window.py" line="119"/>
        <source>Fullscreen mode</source>
        <translation>フルスクリーンモード</translation>
    </message>
    <message>
        <location filename="window.py" line="127"/>
        <source>Quit</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="window.py" line="130"/>
        <source>Undo</source>
        <translation>元に戻す</translation>
    </message>
    <message>
        <location filename="window.py" line="132"/>
        <source>Redo</source>
        <translation>やり直す</translation>
    </message>
    <message>
        <location filename="window.py" line="134"/>
        <source>Copy</source>
        <translation>コピー</translation>
    </message>
    <message>
        <location filename="window.py" line="136"/>
        <source>Cut</source>
        <translation>切り取り</translation>
    </message>
    <message>
        <location filename="window.py" line="138"/>
        <source>Paste</source>
        <translation>貼り付け</translation>
    </message>
    <message>
        <location filename="window.py" line="148"/>
        <source>Enable</source>
        <translation>有効</translation>
    </message>
    <message>
        <location filename="window.py" line="149"/>
        <source>Set locale</source>
        <translation>ロケールの設定</translation>
    </message>
    <message>
        <location filename="window.py" line="150"/>
        <source>Plain text</source>
        <translation>プレーンテキスト</translation>
    </message>
    <message>
        <location filename="window.py" line="152"/>
        <source>Use WebKit renderer</source>
        <translation>WebKitレンダラーを使用</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Generate webpages</source>
        <translation>Webページを生成</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Show</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Next</source>
        <translation>次へ</translation>
    </message>
    <message>
        <location filename="window.py" line="162"/>
        <source>Previous</source>
        <translation>前へ</translation>
    </message>
    <message>
        <location filename="window.py" line="164"/>
        <source>Get help online</source>
        <translation>オンラインでヘルプを読む</translation>
    </message>
    <message>
        <location filename="window.py" line="165"/>
        <source>About %s</source>
        <comment>Example of final string: About ReText</comment>
        <translation>%s について</translation>
    </message>
    <message>
        <location filename="window.py" line="169"/>
        <source>About Qt</source>
        <translation>Qtについて</translation>
    </message>
    <message>
        <location filename="window.py" line="190"/>
        <source>Bold</source>
        <translation>太字</translation>
    </message>
    <message>
        <location filename="window.py" line="192"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="window.py" line="194"/>
        <source>Underline</source>
        <translation>下線</translation>
    </message>
    <message>
        <location filename="window.py" line="202"/>
        <source>Tags</source>
        <translation>タグ</translation>
    </message>
    <message>
        <location filename="window.py" line="206"/>
        <source>Symbols</source>
        <translation>シンボル</translation>
    </message>
    <message>
        <location filename="window.py" line="220"/>
        <source>File</source>
        <translation>ファイル</translation>
    </message>
    <message>
        <location filename="window.py" line="221"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="window.py" line="222"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="window.py" line="225"/>
        <source>Open recent</source>
        <translation>最近使用したファイル</translation>
    </message>
    <message>
        <location filename="window.py" line="229"/>
        <source>Directory</source>
        <translation>ディレクトリ</translation>
    </message>
    <message>
        <location filename="window.py" line="236"/>
        <source>Export</source>
        <translation>エクスポート</translation>
    </message>
    <message>
        <location filename="window.py" line="257"/>
        <source>Spell check</source>
        <translation>スペルチェック</translation>
    </message>
    <message>
        <location filename="window.py" line="268"/>
        <source>Formatting</source>
        <translation>書式</translation>
    </message>
    <message>
        <location filename="window.py" line="306"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <location filename="window.py" line="308"/>
        <source>Case sensitively</source>
        <translation>大文字と小文字を区別</translation>
    </message>
    <message>
        <location filename="window.py" line="1130"/>
        <source>New document</source>
        <translation>新規文書</translation>
    </message>
    <message>
        <location filename="window.py" line="654"/>
        <source>Could not parse file contents, check if you have the &lt;a href=&quot;%s&quot;&gt;necessary module&lt;/a&gt; installed!</source>
        <translation>ファイルの内容を解析できません。&lt;a href=&quot;%s&quot;&gt;必要なモジュール&lt;/a&gt; がインストールされているか確認してください。</translation>
    </message>
    <message>
        <location filename="window.py" line="759"/>
        <source>Please, save the file somewhere.</source>
        <translation>どこかにファイルを保存してください.</translation>
    </message>
    <message>
        <location filename="window.py" line="744"/>
        <source>Failed to copy default template, please create template.html manually.</source>
        <translation>デフォルトのテンプレートをコピーできませんでした。template.html を手動で作成してください。</translation>
    </message>
    <message>
        <location filename="window.py" line="748"/>
        <source>Webpages saved in &lt;code&gt;html&lt;/code&gt; directory.</source>
        <translation>Webページを&lt;code&gt;html&lt;/code&gt;ディレクトリに保存しました.</translation>
    </message>
    <message>
        <location filename="window.py" line="750"/>
        <source>Show directory</source>
        <translation>ディレクトリを表示</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Select one or several files to open</source>
        <translation>開くには一つまたは複数のファイルを選択します</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Supported files</source>
        <translation>サポートされているファイル</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>All files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="window.py" line="956"/>
        <source>Plain text (*.txt)</source>
        <translation>プレーンテキスト (*.txt)</translation>
    </message>
    <message>
        <location filename="window.py" line="959"/>
        <source>%s files</source>
        <comment>Example of final string: Markdown files</comment>
        <translation>%s 個のファイル</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>Save file</source>
        <translation>ファイルを保存</translation>
    </message>
    <message>
        <location filename="window.py" line="978"/>
        <source>Cannot save to file because it is read-only!</source>
        <translation>読み取り専用ファイルなので保存できません!</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>Export document to ODT</source>
        <translation>文書をODTにエクスポート</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>OpenDocument text files (*.odt)</source>
        <translation>OpenDocumentテキストファイル (*.odt)</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>HTML files (*.html *.htm)</source>
        <translation>HTMLファイル (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>Export document to PDF</source>
        <translation>文書をPDFにエクスポート</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDFファイル (*.pdf)</translation>
    </message>
    <message>
        <location filename="window.py" line="1070"/>
        <source>Print document</source>
        <translation>文書を印刷</translation>
    </message>
    <message>
        <location filename="window.py" line="1091"/>
        <source>Export document</source>
        <translation>文書をエクスポート</translation>
    </message>
    <message>
        <location filename="window.py" line="1109"/>
        <source>Failed to execute the command:</source>
        <translation>コマンドの実行に失敗しました:</translation>
    </message>
    <message>
        <location filename="window.py" line="1186"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>文書は変更されています.
変更内容を保存しますか?</translation>
    </message>
    <message>
        <location filename="window.py" line="1212"/>
        <source>HTML code</source>
        <translation>HTMLコード</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Simple but powerful editor for Markdown and reStructuredText</source>
        <translation>Markdown と reStructuredText のためのシンプルでパワフルなエディタ</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Author: Dmitry Shachnev, 2011</source>
        <translation>作者: Dmitry Shachnev, 2011</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Website</source>
        <translation>Webサイト</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Markdown syntax</source>
        <translation>Markdown記法</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>reStructuredText syntax</source>
        <translation>reStructuredText 文法</translation>
    </message>
    <message>
        <location filename="window.py" line="265"/>
        <source>Default markup</source>
        <translation>デフォルトマークアップ</translation>
    </message>
    <message>
        <location filename="window.py" line="580"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="89"/>
        <source>Set encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="931"/>
        <source>Select file encoding from the list:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
