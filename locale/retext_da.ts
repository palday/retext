<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="da" sourcelanguage="">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="config.py" line="18"/>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Automatically save documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore window geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Restore live preview state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open external links in ReText window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Open unknown files in plain text mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Highlight current line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Show line numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tab key inserts spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Tabulation width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Display right margin at column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="config.py" line="18"/>
        <source>Icon theme name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="window.py" line="18"/>
        <source>Enter locale name (example: en_US)</source>
        <translation>Indtast lokalitetsnavn (f.eks. da_DK)</translation>
    </message>
    <message>
        <location filename="window.py" line="23"/>
        <source>Set as default</source>
        <translation>Sæt som standard</translation>
    </message>
</context>
<context>
    <name>ReTextWindow</name>
    <message>
        <location filename="window.py" line="72"/>
        <source>File toolbar</source>
        <translation>Fil-værktøjslinie</translation>
    </message>
    <message>
        <location filename="window.py" line="74"/>
        <source>Edit toolbar</source>
        <translation>Redigering-værktøjslinie</translation>
    </message>
    <message>
        <location filename="window.py" line="76"/>
        <source>Search toolbar</source>
        <translation>Søge-værktøjslinie</translation>
    </message>
    <message>
        <location filename="window.py" line="81"/>
        <source>New</source>
        <translation>Ny</translation>
    </message>
    <message>
        <location filename="window.py" line="84"/>
        <source>Open</source>
        <translation>Åben</translation>
    </message>
    <message>
        <location filename="window.py" line="87"/>
        <source>Save</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="window.py" line="94"/>
        <source>Save as</source>
        <translation>Gem som</translation>
    </message>
    <message>
        <location filename="window.py" line="96"/>
        <source>Print</source>
        <translation>Udskrift</translation>
    </message>
    <message>
        <location filename="window.py" line="99"/>
        <source>Print preview</source>
        <translation>Udskriftsvisning</translation>
    </message>
    <message>
        <location filename="window.py" line="101"/>
        <source>View HTML code</source>
        <translation>Vis HTML-kode</translation>
    </message>
    <message>
        <location filename="window.py" line="102"/>
        <source>Change default font</source>
        <translation>Skift standardskrifttype</translation>
    </message>
    <message>
        <location filename="window.py" line="103"/>
        <source>Find text</source>
        <translation>Find tekst</translation>
    </message>
    <message>
        <location filename="window.py" line="108"/>
        <source>Preview</source>
        <translation>Forhåndsvisning</translation>
    </message>
    <message>
        <location filename="window.py" line="117"/>
        <source>Live preview</source>
        <translation>Live-forhåndsvisning</translation>
    </message>
    <message>
        <location filename="window.py" line="119"/>
        <source>Fullscreen mode</source>
        <translation>Fuldskærmsvisning</translation>
    </message>
    <message>
        <location filename="window.py" line="127"/>
        <source>Quit</source>
        <translation>Afslut</translation>
    </message>
    <message>
        <location filename="window.py" line="130"/>
        <source>Undo</source>
        <translation>Fortryd</translation>
    </message>
    <message>
        <location filename="window.py" line="132"/>
        <source>Redo</source>
        <translation>Gendan</translation>
    </message>
    <message>
        <location filename="window.py" line="134"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="window.py" line="136"/>
        <source>Cut</source>
        <translation>Klip</translation>
    </message>
    <message>
        <location filename="window.py" line="138"/>
        <source>Paste</source>
        <translation>Indsæt</translation>
    </message>
    <message>
        <location filename="window.py" line="148"/>
        <source>Enable</source>
        <translation>Aktiver</translation>
    </message>
    <message>
        <location filename="window.py" line="149"/>
        <source>Set locale</source>
        <translation>Sæt sprogindstillinger</translation>
    </message>
    <message>
        <location filename="window.py" line="150"/>
        <source>Plain text</source>
        <translation>Ren tekst</translation>
    </message>
    <message>
        <location filename="window.py" line="152"/>
        <source>Use WebKit renderer</source>
        <translation>Anvend WebKit-fremviser</translation>
    </message>
    <message>
        <location filename="window.py" line="158"/>
        <source>Generate webpages</source>
        <translation>Generer websider</translation>
    </message>
    <message>
        <location filename="window.py" line="159"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="window.py" line="160"/>
        <source>Next</source>
        <translation>Næste</translation>
    </message>
    <message>
        <location filename="window.py" line="162"/>
        <source>Previous</source>
        <translation>Foregående</translation>
    </message>
    <message>
        <location filename="window.py" line="164"/>
        <source>Get help online</source>
        <translation>Find hjælp online</translation>
    </message>
    <message>
        <location filename="window.py" line="165"/>
        <source>About %s</source>
        <comment>Example of final string: About ReText</comment>
        <translation>Om %s</translation>
    </message>
    <message>
        <location filename="window.py" line="169"/>
        <source>About Qt</source>
        <translation>Om Qt</translation>
    </message>
    <message>
        <location filename="window.py" line="190"/>
        <source>Bold</source>
        <translation>Fed</translation>
    </message>
    <message>
        <location filename="window.py" line="192"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="window.py" line="194"/>
        <source>Underline</source>
        <translation>Understregning</translation>
    </message>
    <message>
        <location filename="window.py" line="202"/>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
    <message>
        <location filename="window.py" line="206"/>
        <source>Symbols</source>
        <translation>Symboler</translation>
    </message>
    <message>
        <location filename="window.py" line="220"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="window.py" line="221"/>
        <source>Edit</source>
        <translation>Rediger</translation>
    </message>
    <message>
        <location filename="window.py" line="222"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="window.py" line="225"/>
        <source>Open recent</source>
        <translation>Åben seneste</translation>
    </message>
    <message>
        <location filename="window.py" line="229"/>
        <source>Directory</source>
        <translation>Mappe</translation>
    </message>
    <message>
        <location filename="window.py" line="236"/>
        <source>Export</source>
        <translation>Eksport</translation>
    </message>
    <message>
        <location filename="window.py" line="257"/>
        <source>Spell check</source>
        <translation>Stavekontrol</translation>
    </message>
    <message>
        <location filename="window.py" line="268"/>
        <source>Formatting</source>
        <translation>Formattering</translation>
    </message>
    <message>
        <location filename="window.py" line="306"/>
        <source>Search</source>
        <translation>Søg</translation>
    </message>
    <message>
        <location filename="window.py" line="308"/>
        <source>Case sensitively</source>
        <translation>Skelnen mellem store og små bogstaver</translation>
    </message>
    <message>
        <location filename="window.py" line="1130"/>
        <source>New document</source>
        <translation>Nyt dokument</translation>
    </message>
    <message>
        <location filename="window.py" line="654"/>
        <source>Could not parse file contents, check if you have the &lt;a href=&quot;%s&quot;&gt;necessary module&lt;/a&gt; installed!</source>
        <translation>Kunne ikke indlæse filens indhold, dobbeltcheck at du har det &lt;a href=&quot;%s&quot;&gt;nødvendige modul&lt;/a&gt; installeret.</translation>
    </message>
    <message>
        <location filename="window.py" line="759"/>
        <source>Please, save the file somewhere.</source>
        <translation>Gem venligst filen et sted.</translation>
    </message>
    <message>
        <location filename="window.py" line="744"/>
        <source>Failed to copy default template, please create template.html manually.</source>
        <translation>Kunne ikke kopiere standardskabelonen. Opret template.html manuelt.</translation>
    </message>
    <message>
        <location filename="window.py" line="748"/>
        <source>Webpages saved in &lt;code&gt;html&lt;/code&gt; directory.</source>
        <translation>Sider gemt i &lt;code&gt;html&lt;/code&gt;-mappe.</translation>
    </message>
    <message>
        <location filename="window.py" line="750"/>
        <source>Show directory</source>
        <translation>Vis mappe</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Select one or several files to open</source>
        <translation>Vælg en eller flere filer, der skal åbnes</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>Supported files</source>
        <translation>Understøttede filer</translation>
    </message>
    <message>
        <location filename="window.py" line="874"/>
        <source>All files (*)</source>
        <translation>Alle filer (*)</translation>
    </message>
    <message>
        <location filename="window.py" line="956"/>
        <source>Plain text (*.txt)</source>
        <translation>Ren tekst (*.txt)</translation>
    </message>
    <message>
        <location filename="window.py" line="959"/>
        <source>%s files</source>
        <comment>Example of final string: Markdown files</comment>
        <translation>%s-filer</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>Save file</source>
        <translation>Gem fil</translation>
    </message>
    <message>
        <location filename="window.py" line="978"/>
        <source>Cannot save to file because it is read-only!</source>
        <translation>Kan ikke gemme filen, fordi den er skrivebeskyttet!</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>Export document to ODT</source>
        <translation>Eksporter dokument til ODT</translation>
    </message>
    <message>
        <location filename="window.py" line="1023"/>
        <source>OpenDocument text files (*.odt)</source>
        <translation>OpenDocument-tekstfiler (*.odt)</translation>
    </message>
    <message>
        <location filename="window.py" line="1033"/>
        <source>HTML files (*.html *.htm)</source>
        <translation>HTML-filer (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>Export document to PDF</source>
        <translation>Eksporter dokument til PDF</translation>
    </message>
    <message>
        <location filename="window.py" line="1054"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-filer (*.pdf)</translation>
    </message>
    <message>
        <location filename="window.py" line="1070"/>
        <source>Print document</source>
        <translation>Udskriv dokument</translation>
    </message>
    <message>
        <location filename="window.py" line="1091"/>
        <source>Export document</source>
        <translation>Eksporter dokument</translation>
    </message>
    <message>
        <location filename="window.py" line="1109"/>
        <source>Failed to execute the command:</source>
        <translation>Kunne ikke udføre kommandoen:</translation>
    </message>
    <message>
        <location filename="window.py" line="1186"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Dokumentet er blevet ændret.
Ønsker du at gemme dine ændringer?</translation>
    </message>
    <message>
        <location filename="window.py" line="1212"/>
        <source>HTML code</source>
        <translation>HTML-kode</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Simple but powerful editor for Markdown and reStructuredText</source>
        <translation>Enkel men stærk editor til Markdown og reStructuredText</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Author: Dmitry Shachnev, 2011</source>
        <translation>Forfatter: Dmitry Shachnev, 2011</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Website</source>
        <translation>Hjemmeside</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>Markdown syntax</source>
        <translation>Markdown-syntaks</translation>
    </message>
    <message>
        <location filename="window.py" line="1223"/>
        <source>reStructuredText syntax</source>
        <translation>reStructuredText-syntaks</translation>
    </message>
    <message>
        <location filename="window.py" line="265"/>
        <source>Default markup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="580"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="89"/>
        <source>Set encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.py" line="931"/>
        <source>Select file encoding from the list:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
